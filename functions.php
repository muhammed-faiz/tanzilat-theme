<?php
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles', 3 );
function theme_enqueue_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

add_action('wp_ajax_show_code', 'couponxxl_show_code');
add_action('wp_ajax_nopriv_show_code', 'couponxxl_show_code');
function couponxxl_show_code(){

	$offer_id = esc_sql( $_POST['offer_id'] );
	couponxxl_register_click( $offer_id );
	$offer = get_post( $offer_id );
	$offer_modal = '';
	if( !empty( $offer ) ){
		$offer_store = get_post_meta( $offer_id, 'offer_store', true );
		$coupon_type = get_post_meta( $offer_id, 'coupon_type', true );

		$store_link = get_post_meta( $offer_store, 'store_link', true );
		?>
		<?php if( !empty( $store_link ) ): ?>
			<a href="<?php echo esc_url( add_query_arg( array( 'rs' => $offer_store ), get_permalink() ) ) ?>" target="_blank">
		<?php endif; ?>
			<?php couponxxl_store_logo( $offer_store ); ?>
		<?php if( !empty( $store_link ) ): ?>
			</a>
		<?php endif; ?>			

		<h4><?php echo $offer->post_title; ?></h4>
		<?php
		if( $coupon_type == 'code' ){
			$coupon_code = is_user_logged_in()
				? get_post_meta( $offer_id, 'coupon_code', true )
				: __( 'Registered Users Only!', 'couponxxl' )
			;
			echo '<input type="text" class="btn coupon-code-modal" readonly="readonly" value="'.esc_attr( $coupon_code ).'" />';
			if (!is_user_logged_in()) {
				echo '<p><strong>'.'<a href="#login" data-toggle="modal">'.esc_html__( 'Login', 'couponxxl' ).'</a> '.esc_html__( 'or', 'couponxxl' ).' <a href="'.esc_url( couponxxl_get_permalink_by_tpl( 'page-tpl_register' ) ).'">'.esc_html__( 'Register', 'couponxxl' ).'</a>'.'</strong></p>';

			} else {
				echo '<p class="coupon-code-copied" data-aftertext="'.esc_html__( 'Code is copied.', 'couponxxl' ).'">'.esc_html__( 'Click the code to auto copy.', 'couponxxl' ).'</p>';
			}
		}
		else if( $coupon_type == 'printable' ){
			$coupon_image = get_post_meta( $offer_id, 'coupon_image', true );
			echo wp_get_attachment_image( $coupon_image, 'full', 0, array( 'class' => 'coupon-print-image' ) );
			echo '<a class="btn coupon-code-modal print" href="javascript:print();">'.esc_html__( 'PRINT', 'couponxxl' ).'</a>';
		}
		else if( $coupon_type == 'sale' ){
			$coupon_sale = get_post_meta( $offer_id, 'coupon_sale', true );
			echo '<a class="btn coupon-code-modal" href="'.esc_url( $coupon_sale ).'" target="_blank">'.esc_html__( 'SEE SALE', 'couponxxl' ).'</a>';
		}		
		?>

		<?php echo couponxxl_thumbs_html( $offer_id ); ?>


		<?php 
		$coupon_modal_content = couponxxl_get_option( 'coupon_modal_content' );
		if( $coupon_modal_content == 'content' ){
			echo apply_filters( 'the_content', $offer->post_content );	
		}
		else{
			echo $offer->post_excerpt;	
		}
		?>

		<div class="code-footer">
			<a href="<?php echo get_permalink( $offer_store ) ?>">
				<?php echo esc_html__( 'See all ', 'couponxxl' ).get_the_title( $offer_store ).esc_html__( ' Coupons & Deals', 'couponxxl' ); ?>
			</a>

			<ul class="list-unstyled list-inline store-social-networks">
				<li>
					<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode( get_permalink( $offer_id ) ) ?>" class="share" target="_blank">
						<i class="fa fa-facebook-square"></i>
					</a>
				</li>
				<li>
					<a href="http://twitter.com/intent/tweet?source=<?php echo esc_attr( get_bloginfo('name') ) ?>&amp;text=<?php echo urlencode( get_permalink( $offer_id ) ) ?>" class="share" target="_blank">
						<i class="fa fa-twitter-square"></i>
					</a>
				</li>
				<li>
					<a href="https://plus.google.com/share?url=<?php echo urlencode( get_permalink( $offer_id ) ) ?>" class="share" target="_blank">
						<i class="fa fa-google-plus-square"></i>
					</a>
				</li>

				<li>
					<script>var pfHeaderImgUrl = '';var pfHeaderTagline = '';var pfdisableClickToDel = 0;var pfHideImages = 0;var pfImageDisplayStyle = 'right';var pfDisablePDF = 0;var pfDisableEmail = 0;var pfDisablePrint = 0;var pfCustomCSS = '';var pfBtVersion='1';(function(){var js, pf;pf = document.createElement('script');pf.type = 'text/javascript';if ('https:' === document.location.protocol){js='https://pf-cdn.printfriendly.com/ssl/main.js'}else{js='http://cdn.printfriendly.com/printfriendly.js'}pf.src=js;document.getElementsByTagName('head')[0].appendChild(pf)})();</script>
						<a href="http://www.printfriendly.com" style="color:#6D9F00;text-decoration:none;" class="printfriendly" onclick="window.print();return false;" title="Print"><i class="fa fa-print"></i></a>
				</li>

				<li>
					<a href="mailto:someone@gmail.com?subject=<?php echo $offer->post_title; ?>&amp;body=<?php echo $offer->post_title; ?>%0D%0A%0D%0A<?php echo get_permalink($offer_id); ?>"><i class="fa fa-envelope"></i></a>
				</li>
			</ul>
		</div>
		<?php
	}
	die();
}
?>